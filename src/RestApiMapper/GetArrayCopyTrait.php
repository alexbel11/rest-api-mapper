<?php
/**
 * Created by zdi design group
 * http://www.zdidesigngroup.com
 *
 * User: derekmiranda
 * Date: 7/23/14
 * Time: 11:05 AM
 * Project: rest-api-mapper
 */
namespace RestApiMapper;

/**
 * Class GetArrayCopyTrait
 * @package RestApiMapper
 */
trait GetArrayCopyTrait {

    /**
     * @return array
     */
    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

    /**
     * @return array
     */
    public function extract()
    {
        return $this->getArrayCopy();
    }
}