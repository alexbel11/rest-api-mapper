<?php
namespace RestApiMapper;

use OAuth2\Request;
use Zend\Filter\FilterChain;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use ZF\ApiProblem\ApiProblem;
use Filter;
use ZF\MvcAuth\Identity\AuthenticatedIdentity;
use ZF\MvcAuth\Identity\GuestIdentity;

class Mapper implements ServiceLocatorAwareInterface
{
    /**
     * Service Locator
     * @var ServiceLocatorInterface
     */
    protected $serviceLocator;

    /**
     * @var FilterChain
     */
    protected $digitFilterChain;

    /**
     * @var FilterChain
     */
    protected $stringFilterChain;

    /**
     * @var Filter\IdFilter
     */
    protected $idFilter;

    /**
     * @var array
     */
    protected $oauthIdentity;


    protected $authService;

    protected function getAuthService()
    {
        if(!isset($this->authService)){
            $this->authService = $this->getServiceLocator()->get('Application\Authentication');
        }
        return $this->authService;
    }

    /**
     * Set service locator
     *
     * @param ServiceLocatorInterface $serviceLocator
     */
    public function setServiceLocator(ServiceLocatorInterface $serviceLocator)
    {
        $this->serviceLocator = $serviceLocator;
    }

    /**
     * Get service locator
     *
     * @return ServiceLocatorInterface
     */
    public function getServiceLocator()
    {
        return $this->serviceLocator;
    }

    /**
     * Create a resource
     *
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function create($data)
    {
        return new ApiProblem(405, 'The POST method has not been defined');
    }

    /**
     * Delete a resource
     *
     * @param  mixed $id
     * @return ApiProblem|mixed
     */
    public function delete($id)
    {
        return new ApiProblem(405, 'The DELETE method has not been defined for individual resources');
    }

    /**
     * Delete a collection, or members of a collection
     *
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function deleteList($data)
    {
        return new ApiProblem(405, 'The DELETE method has not been defined for collections');
    }

    /**
     * Fetch a resource
     *
     * @param  mixed $id
     * @return ApiProblem|mixed
     */
    public function fetch($id)
    {
        return new ApiProblem(405, 'The GET method has not been defined for individual resources');
    }

    /**
     * Fetch all or a subset of resources
     *
     * @param  array $params
     * @return ApiProblem|mixed
     */
    public function fetchAll($params = array())
    {
        return new ApiProblem(405, 'The GET method has not been defined for individual collections');
    }

    /**
     * Patch (partial in-place update) a resource
     *
     * @param  mixed $id
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function patch($id, $data)
    {
        return new ApiProblem(405, 'The PATCH method has not been defined for individual resources');
    }

    /**
     * Patch (partial in-place update) a collection or members of a collection
     *
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function patchList($data)
    {
        return new ApiProblem(405, 'The PATCH method has not been defined for collections');
    }

    /**
     * Replace a collection or members of a collection
     *
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function replaceList($data)
    {
        return new ApiProblem(405, 'The PUT method has not been defined for collections');
    }

    /**
     * Update a resource
     *
     * @param  mixed $id
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function update($id, $data)
    {
        return new ApiProblem(405, 'The PUT method has not been defined for individual resources');
    }

    /**
     * Filters digits
     * @param $digits
     * @return int
     */
    public function filterDigit($digits)
    {
        return $this->getDigitFilterChain()->filter($digits);
    }

    /**
     * Gets the Digit Filter Chain
     * @return FilterChain
     */
    protected function getDigitFilterChain()
    {
        if( $this->digitFilterChain == null )
        {
            $this->digitFilterChain = new FilterChain();
            $this->digitFilterChain->attachByName('Digits');
        }

        return $this->digitFilterChain;
    }

    /**
     * Filters strings
     * @param $string
     * @return string
     */
    public function filterString($string)
    {
        return $this->getStringFilterChain()->filter($string);
    }

    /**
     * Gets the String Filter Chain
     * @return FilterChain
     */
    protected function getStringFilterChain()
    {
        if( $this->stringFilterChain == null )
        {
            $this->stringFilterChain = new FilterChain();
            $this->stringFilterChain->attachByName('StripNewLines');
            $this->stringFilterChain->attachByName('StripTags');
            $this->stringFilterChain->attachByName('StringTrim');
        }

        return $this->stringFilterChain;
    }

    /**
     * Filter an ID to an int
     * @param $id
     * @return mixed
     */
    public function filterId($id)
    {
        if( $this->idFilter == null )
        {
            $this->idFilter = $this->getServiceLocator()->get('FilterManager')->get('Filter\IdFilter');
        }

        return $this->idFilter->filter($id);
    }

    /**
     * Gets the credentials from the oauth request
     * @return array
     */
    public function getOAuthIdentity()
    {
        if ($this->oauthIdentity == null)
        {
            $this->oauthIdentity = $this->getServiceLocator()->get('api-identity');
        }

        return $this->oauthIdentity;
    }

    public function authorized($scope,$resource_id){
        if(!is_null($resource_id)){
            $resource_id = intval($resource_id);
        }

        $identity = $this->getOAuthIdentity();

        if( $identity instanceof GuestIdentity && $scope !== 'guest' )
        {
            // we have a guest and the scope isn't for guest
            return false;
        }

        if( $identity instanceof AuthenticatedIdentity )
        {
            $identity = $identity->getAuthenticationIdentity();
        }

        // if admin - true
        if($identity['scope'] == 'admin'){
            return true;
        }
        // if scope doesn't match - false
        if($identity['scope'] != $scope){
            return false;
        }

        // if no resource, just a scope check, return true
        if(is_null($resource_id)){
            return true;
        }

        $ids = $this->getAuthService()->getUserTypeIds($identity);
        switch ($scope)
        {
            case "merchant":
                if(in_array(array('merchant_id'=>$resource_id)  ,$ids['merchants_ids'])) {
                    return true;
                }else{
                    return false;
                }
                break;
            case "customer":
                if($resource_id == $ids['customers_id']) {
                    return true;
                }else{
                    return false;
                }
                break;
            default:
                return false;
        }
    }

    /**
     * Tests if the person accessing the API has permission to edit the resource
     * @param string $scope  The users scope
     * @param null $userId  The user Id to test
     * @param string $userIdKey  The array key to check in the oauth identity
     * @return bool
     */
    public function isAllowed($scope, $userId = null, $userIdKey = 'user_id')
    {
        $identity = $this->getOAuthIdentity();


        if( $identity instanceof GuestIdentity && $scope !== 'guest' )
        {
            // we have a guest and the scope isn't for guest
            return false;
        }

        if( $identity instanceof AuthenticatedIdentity )
        {
            $identity = $identity->getAuthenticationIdentity();
        }

        // admin scopes are always allowed
        if( $identity['scope'] == 'admin' )
        {
            return true;
        }

        // if the user doesn't belong to the scope we are qualifying, they are not allowed
        if( $scope != $identity['scope'] )
        {
            return false;
        }

        // if the user is absolutely the resource owner
        if( $identity[$userIdKey] === $userId .'?' . $scope )
        {
            return true;
        }

        // no user id was passed, so just qualify it by the scope
        if( $userId == null && $scope == $identity['scope'])
        {
            return true;
        }

        return false;
    }

    /**
     * Debug function for dumping SQL statements
     * @param Select $select
     * @param TableGateway $table
     * @param bool $die
     */
    protected function debugSqlStatement($select, $table, $die = true)
    {
        echo $select->getSqlString($table->getAdapter()->getPlatform());

        if($die)
        {
            die();
        }
    }
}