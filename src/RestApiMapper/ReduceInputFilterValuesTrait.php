<?php
/**
 * Created by PhpStorm.
 * User: derekmiranda
 * Date: 6/15/14
 * Time: 5:41 PM
 */

namespace RestApiMapper;

/**
 * Class ReduceInputFilterValuesTrait
 * @package Application\ResourceListener
 */
trait ReduceInputFilterValuesTrait {

    /**
     * @param $passedDataArray
     * @return array
     */
    protected function reduceValues($passedDataArray)
    {
        $reducedData = array();
        $inputFilterValues = $this->getInputFilter()->getValues();

        foreach(get_object_vars($passedDataArray) as $key => $rawValue)
        {
            if (isset($inputFilterValues[$key]))
            {
                $reducedData[$key] = $inputFilterValues[$key];
            }
        }

        return $reducedData;
    }
}
